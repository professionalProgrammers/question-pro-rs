use rand::prelude::*;
use reqwest::Client as ReqwestClient;
use select::document::Document;
use select::predicate::*;
use url::Url;

pub type PResult<T> = Result<T, PError>;

#[derive(Debug)]
pub enum PError {
    Network,
    InvalidBody,
    InvalidUrl,
}

#[derive(Debug)]
struct SurveyUrl(Url);

#[derive(Debug)]
struct Survey {
    id: String,
}

impl Survey {
    pub fn get_url(&self) -> String {
        let mut rng = rand::thread_rng();
        format!(
            "https://www.questionpro.com/a/TakePoll?pollID={}&qindex={}&_currentInset={}&rnd={}",
            self.id,
            1,
            0,
            rng.gen::<f32>()
        )
    }
}

struct Client {
    client: ReqwestClient,
}

impl Client {
    pub fn new() -> Self {
		let mut headers = reqwest::header::HeaderMap::new();
		headers.insert(reqwest::header::USER_AGENT, reqwest::header::HeaderValue::from_static("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"));
		
        let client = ReqwestClient::builder()
            .cookie_store(false)
			//.default_headers(headers)
            .build()
            .unwrap();

        Client { client }
    }

    pub fn get_survey(&self, url: &SurveyUrl) -> PResult<Survey> {
        let _res = self
            .client
            .get(url.0.as_str())
            .send()
            .map_err(|_| PError::Network)?
            .text()
            .map_err(|_| PError::InvalidBody)?;
        //dbg!(res);
        let id = url
            .0
            .query_pairs()
            .find_map(|(k, v)| match (&*k, &*v) {
                ("pollID", id) => Some(id.to_string()),
                _ => None,
            })
            .ok_or(PError::InvalidUrl)?;
        Ok(Survey { id })
    }

    pub fn submit_survey(&self, survey: &Survey) -> PResult<()> {		
        let res = self
            .client
            .get(survey.get_url().as_str())
            .send()
            .map_err(|_| PError::Network)?
            .text()
            .map_err(|_| PError::InvalidBody)?;
		
        println!("{}", res);
        Ok(())
    }

    pub fn find_survey_urls(&self) -> PResult<Vec<SurveyUrl>> {
        let url = "https://www.sacbee.com/sports/high-school/article235438967.html";
        let res = self.client.get(url).send().map_err(|_| PError::Network)?;

        let doc = Document::from_read(res).map_err(|_| PError::InvalidBody)?;
        Ok(doc
            .find(And(Name("script"), Attr("src", ())))
            .filter_map(|el| el.attr("src").and_then(|src| Url::parse(src).ok()))
            .filter(|url| url.host_str() == Some("www.questionpro.com"))
            .filter(|url| url.path() == "/a/TakePoll")
            .filter(|url| url.query().is_some())
            .map(|url| SurveyUrl(url))
            .collect())
    }
}

fn main() {
    let client = Client::new();
    let surveys = client.find_survey_urls().unwrap();
    let survey = client.get_survey(&surveys.first().unwrap()).unwrap();

    dbg!(&survey);

    for _ in 0..100 {
        client.submit_survey(&survey);
    }

    println!("Hello, world!");
}
